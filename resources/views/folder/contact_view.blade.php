@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="{{ url('folder/'.$folder->slug) }}">{{ $folder->name }}</a> > {{ $contact->fullname }} <a class="pull-right" href="{{ url('folder/'.$folder->slug.'/'.$contact->slug.'/remove') }}"><i class="fa fa-times"></i> Verwijderen</a></div>

                <div class="panel-body">
                    <form method="POST">
                    	{!! csrf_field() !!}
                    	<label for="name_first">Voornaam:</label>
                    	<input type="text" class="form-control" id="name_first" name="name_first" value="{{ $contact->name_first }}" />
                        <label for="name_last">Achternaam:</label>
                        <input type="text" class="form-control" id="name_last" name="name_last" value="{{ $contact->name_last }}" />
                        <label for="company">Bedrijf:</label>
                        <input type="text" class="form-control" id="company" name="company" value="{{ $contact->company }}" />
                        <label for="role">Functie:</label>
                        <input type="text" class="form-control" id="role" name="role" value="{{ $contact->role }}" />

                        <label for="phone_home">Telefoonnummer thuis:</label>
                        <input type="text" class="form-control" id="phone_home" name="phone_home" value="{{ $contact->phone_home }}" />
                        <label for="phone_work">Telefoonnummer werk:</label>
                        <input type="text" class="form-control" id="phone_work" name="phone_work" value="{{ $contact->phone_work }}" />
                        <label for="phone_mobile">Telefoonnummer mobiel:</label>
                        <input type="text" class="form-control" id="phone_mobile" name="phone_mobile" value="{{ $contact->phone_mobile }}" />

                        <label for="email_address">E-mail adres:</label>
                        <input type="text" class="form-control" id="email_address" name="email_address" value="{{ $contact->email_address }}" />

                        <label for="address_address">Adres:</label>
                        <input type="text" class="form-control" id="address_address" name="address_address" value="{{ $contact->address_address }}" />
                        <label for="address_postalcode">Postcode:</label>
                        <input type="text" class="form-control" id="address_postalcode" name="address_postalcode" value="{{ $contact->address_postalcode }}" />
                        <label for="address_city">Plaats:</label>
                        <input type="text" class="form-control" id="address_city" name="address_city"  value="{{ $contact->address_city }}"/>
                        <label for="address_country">Land:</label>
                        <input type="text" class="form-control" id="address_country" name="address_country"  value="{{ $contact->address_country }}"/>

                        <label for="notes">Notities:</label>
                        <textarea class="form-control" id="notes" name="notes">{{ $contact->notes }}</textarea>
                    	<br />
                    	<input type="submit" class="pull-right btn btn-primary" value="Opslaan" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
