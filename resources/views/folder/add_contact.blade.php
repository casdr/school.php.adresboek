@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Contact toevoegen</div>

                <div class="panel-body">
                    <form method="POST">
                    	{!! csrf_field() !!}
                    	<label for="name_first">Voornaam:</label>
                    	<input type="text" class="form-control" id="name_first" name="name_first"/>
                        <label for="name_last">Achternaam:</label>
                        <input type="text" class="form-control" id="name_last" name="name_last" />
                        <label for="company">Bedrijf:</label>
                        <input type="text" class="form-control" id="company" name="company" />
                        <label for="role">Functie:</label>
                        <input type="text" class="form-control" id="role" name="role" />

                        <label for="phone_home">Telefoonnummer thuis:</label>
                        <input type="text" class="form-control" id="phone_home" name="phone_home" />
                        <label for="phone_work">Telefoonnummer werk:</label>
                        <input type="text" class="form-control" id="phone_work" name="phone_work" />
                        <label for="phone_mobile">Telefoonnummer mobiel:</label>
                        <input type="text" class="form-control" id="phone_mobile" name="phone_mobile" />

                        <label for="email_address">E-mail adres:</label>
                        <input type="text" class="form-control" id="email_address" name="email_address" />

                        <label for="address_address">Adres:</label>
                        <input type="text" class="form-control" id="address_address" name="address_address" />
                        <label for="address_postalcode">Postcode:</label>
                        <input type="text" class="form-control" id="address_postalcode" name="address_postalcode" />
                        <label for="address_city">Plaats:</label>
                        <input type="text" class="form-control" id="address_city" name="address_city" />
                        <label for="address_country">Land:</label>
                        <input type="text" class="form-control" id="address_country" name="address_country" />

                        <label for="notes">Notities:</label>
                        <textarea class="form-control" id="notes" name="notes"></textarea>
                    	<br />
                    	<input type="submit" class="pull-right btn btn-primary" value="Toevoegen" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
