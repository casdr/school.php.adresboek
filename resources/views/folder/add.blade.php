@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Contact map toevoegen</div>

                <div class="panel-body">
                    <form method="POST" action="{{ url('folder/add') }}">
                    	{!! csrf_field() !!}
                    	<label for="name">Naam:</label>
                    	<input type="text" class="form-control" id="name" name="name" />
                    	<br />
                    	<input type="submit" class="pull-right btn btn-primary" value="Toevoegen" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
