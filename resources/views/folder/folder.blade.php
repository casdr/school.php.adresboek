@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $folder->name }} <a class="pull-right" href="{{ url('folder/'.$folder->slug.'/add') }}"><i class="fa fa-plus"></i> Contact toevoegen</a></div>

                <div class="panel-body">
                    <div class="panel-body">
	                    <ul class="list-group">
	                    @foreach($folder->contacts as $c)
	                      <li class="list-group-item"><a href="{{ url('folder/'.$folder->slug . '/' . $c->slug) }}">{{ $c->fullname }}</a></li>
	                    @endforeach
	                    </ul>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
