@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Contact mappen <a class="pull-right" href="{{ url('folder/add') }}"><i class="fa fa-plus"></i> Toevoegen</a></div>

                <div class="panel-body">
                    <ul class="list-group">
                    @foreach($folders as $f)
                        <li class="list-group-item"><a href="{{ url('folder/'.$f->slug) }}">{{ $f->name }}</a></li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
