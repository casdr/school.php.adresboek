<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_first')->nullable();
            $table->string('name_last')->nullable();
            $table->string('company')->nullable();
            $table->string('role')->nullable();

            $table->string('phone_home')->nullable();
            $table->string('phone_work')->nullable();
            $table->string('phone_mobile')->nullable();

            $table->string('email_address')->nullable();

            $table->string('address_address')->nullable();
            $table->string('address_postalcode')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_country')->nullable();

            $table->text('notes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
