<?php
namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class ContactFolder extends Model implements SluggableInterface {

	use SluggableTrait;

	protected $table = 'contact_folders';

	protected $fillable = [
		'name', 'user_id'
	];

	protected $sluggable = [
		'build_from' => 'name'
	];

	public function contacts() {
		return $this->hasMany('App\Models\Contact', 'folder_id');
	}
}