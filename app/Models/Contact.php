<?php
namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model implements SluggableInterface {

	use SluggableTrait;

	protected $table = 'contacts';

	protected $fillable = [
		'name_first', 'name_last',
		'company', 'role',
		'phone_home', 'phone_work', 'phone_mobile',
		'email_address',
		'address_address', 'address_postalcode', 'address_city', 'address_country',
		'notes'
	];

	protected $sluggable = [
		'build_from' => 'fullname'
	];

	public function getFullnameAttribute() {
		return $this->name_first . ' ' . $this->name_last;
	}

}