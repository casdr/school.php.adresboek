<?php
/**
 * Application routes
 */



Route::auth();

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'HomeController@index');

	Route::get('folder/add', 'FolderController@getAdd');
	Route::post('folder/add', 'FolderController@postAdd');

	Route::get('folder/{folder}', 'FolderController@getFolder');
	
	Route::get('folder/{folder}/add', 'FolderController@getAddContact');
	Route::post('folder/{folder}/add', 'FolderController@postAddContact');
	Route::get('folder/{folder}/{contact}', 'FolderController@getContact');
	Route::post('folder/{folder}/{contact}', 'FolderController@postContact');
	Route::get('folder/{folder}/{contact}/remove', 'FolderController@removeContact');
});
