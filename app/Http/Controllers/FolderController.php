<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\ContactFolder;
use App\Models\Contact;
use Auth;
use Illuminate\Http\Request;

class FolderController extends Controller
{
    public function getAdd() {
    	return view('folder.add');
    }
    public function postAdd(Request $request) {
    	$created = ContactFolder::create([
    		'name' => $request->input('name'),
    		'user_id' => Auth::user()->id
    	]);
    	return redirect('folder/' . $created->slug)->with('message', 'De map ' . $request->input('name') . ' is aangemaakt');
    }

    public function getFolder(Request $request, $folder) {
    	$folder = ContactFolder::findBySlug($folder);

    	if(!$folder) {
    		return redirect('/')->with('error', 'Deze map bestaat niet');
    	}

    	return view('folder.folder', ['folder' => $folder]);
    }

    public function getAddContact($folder) {
        $folder = ContactFolder::findBySlug($folder);

        if(!$folder) {
            return redirect('/')->with('error', 'Deze map bestaat niet');
        }

        return view('folder.add_contact');
    }
    public function postAddContact(Request $request, $folder) {
        $folder = ContactFolder::findBySlug($folder);

        if(!$folder) {
            return redirect('/')->with('error', 'Deze map bestaat niet');
        }

        $contact = $folder->contacts()->create($request->all());
        return redirect('folder/'.$folder->slug.'/'.$contact->slug)->with('message', 'Deze contact is aangemaakt');
    }

    public function getContact(Request $request, $folder, $contact) {
        $folder = ContactFolder::findBySlug($folder);

        if(!$folder) {
            return redirect('/')->with('error', 'Deze map bestaat niet');
        }

        $contact = Contact::findBySlug($contact);

        if(!$contact) {
            return redirect('folder/'.$folder->slug)->with('error', 'Deze contact bestaat niet');
        }

        return view('folder.contact_view', ['folder' => $folder, 'contact' => $contact]);
    }

    public function postContact(Request $request, $folder, $contact) {
        $folder = ContactFolder::findBySlug($folder);

        if(!$folder) {
            return redirect('/')->with('error', 'Deze map bestaat niet');
        }

        $contact = Contact::findBySlug($contact);

        if(!$contact) {
            return redirect('folder/'.$folder->slug)->with('error', 'Deze contact bestaat niet');
        }

        $contact->update($request->all());

        return redirect('folder/'.$folder->slug.'/'.$contact->slug)->with('message', 'Deze contact is geupdated');
    }

    public function removeContact(Request $request, $folder, $contact) {
        $folder = ContactFolder::findBySlug($folder);

        if(!$folder) {
            return redirect('/')->with('error', 'Deze map bestaat niet');
        }

        $contact = Contact::findBySlug($contact);

        $contact->delete();

        return redirect('folder/'.$folder->slug)->with('message', 'De contact ' . $contact->fullname . ' is verwijderd.');
    }
}
